
import java.util.Scanner;
import java.lang.Math;

public class problem2 {
    public static void main(String []args) {
        int num, digit;

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a five digit number");
        num = scan.nextInt();

        for (int i=4; i>=0; i--){
            digit = num / (int)Math.pow(10, i);
            num = num % (int)Math.pow(10, i);
            System.out.print(digit + "   ");
        }

    }
}
