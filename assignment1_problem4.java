import java.util.Scanner;

public class problem4 {
    public static void main(String []args) {
        int hours;
        double rate, netpay;
        String name;
        Scanner scan = new Scanner(System.in);

        for(int i=0; i<3; i++){
            System.out.println("Enter your first name");
            name = scan.next();
            System.out.println("Enter how many hour you have worked");
            hours = scan.nextInt();
            System.out.println("Enter your hourly rate pay ");
            rate = scan.nextDouble();

            if(hours > 40)
            {
                netpay = (40 * rate) + (0.5 * (hours - 40) * rate);
                System.out.println("Hello " + name + ", your net pay for the week is " + netpay + "\n\n");
            }
            else if(hours >= 0)
            {
                netpay = (hours * rate);
                System.out.println("Hello " + name + ", your net pay for the week is " + netpay + "\n\n");
            }

            else {
                System.out.println("Please enter a valid number of hours");
            }
        }
    }
}
